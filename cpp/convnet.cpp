
#define training_samples 5000
#define testing_samples 1000

//bin/cat /dev/null; : <<'a'

/*
a

outfile="$0.out"
g++ -O3 -Wall -Wextra "$0" -o "$outfile"
"$outfile" "$@"
status=$?
rm "$outfile"
exit $status

*/

/**
 * neural_network.cpp -- optimised simulation of neural networks
 */

#include <math.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdint.h>

// Enable easy switching between single- and double-precision:
typedef double real_t;



struct mnist_digit {

    public:
    int8_t imagedata[625];
    int actual_digit;

    void realise(real_t *arr) {

        for (int i = 0; i < 625; i++) {
            arr[i] = 0.01 * imagedata[i];
        }
    }

    void realise_target(real_t *arr) {

        for (int i = 0; i < 10; i++) {
            arr[i] = (i == actual_digit) ? 1.0 : -1.0; 
        }
    }

    void populate_from_file(std::ifstream &infile) {

        std::string s;

        std::getline(infile, s);
        actual_digit = s.c_str()[14] - 48;

        for (int i = 0; i < 25; i++) {
            std::getline(infile, s);
            for (int j = 0; j < 25; j++) {
                int tens = s.c_str()[2*j] - 48;
                int units = s.c_str()[2*j+1] - 48;
                imagedata[25 * i + j] = 10 * tens + units;
            }
        }

        std::getline(infile, s);
    }

};

void boxmuller(real_t *arr, int length, double stdev) {

    // Populate the array with normal variables:
    for (int i = 0; i < length; i += 2) {
        double angle = (6.2831853 * rand()) / RAND_MAX;
        double radius = stdev * sqrt(-2.0 * log((0.999998 * rand()) / RAND_MAX + 0.000001));
        arr[i] = radius * cos(angle);
        if ((i + 1) < length) {
            arr[i+1] = radius * sin(angle);
        }
    }

}


class ILayer {

    public:
    virtual void feedforward(real_t *a, real_t *z) = 0;
    virtual void backprop(real_t *a, real_t *dC_dz, real_t *dC_da) = 0;

};


class BabbageSigmoid : public ILayer {

    int arity;

    public:
    void feedforward(real_t *a, real_t *z) {

        for (int i = 0; i < arity; i++) {
            real_t w = a[i] + (2.0 * a[i] * a[i] * a[i]);
            z[i] = w / (1.0 + fabs(w));
        }

    }

    void backprop(real_t *a, real_t *dC_dz, real_t *dC_da) {

        for (int i = 0; i < arity; i++) {
            real_t w = a[i] + (2.0 * a[i] * a[i] * a[i]);
            real_t denom = 1.0 + fabs(w);
            real_t dw_da = 1.0 + 6.0 * a[i] * a[i];
            real_t dz_da = dw_da / (denom * denom);
            dC_da[i] = dz_da * dC_dz[i];
        }

    }

    BabbageSigmoid(int ar) {

        arity = ar;

    }

};


class PoorSigmoid : public ILayer {

    int arity;

    public:
    void feedforward(real_t *a, real_t *z) {

        for (int i = 0; i < arity; i++) {
            z[i] = a[i] / (1.0 + fabs(a[i]));
        }

    }

    void backprop(real_t *a, real_t *dC_dz, real_t *dC_da) {

        for (int i = 0; i < arity; i++) {
            real_t denom = 1.0 + fabs(a[i]);
            real_t dz_da = 1.0 / (denom * denom);
            dC_da[i] = dz_da * dC_dz[i];
        }

    }

    PoorSigmoid(int ar) {

        arity = ar;

    }

};


class MaxPool : public ILayer {

    int in_width;
    int out_width;

    int in_height;
    int out_height;

    int pool_size;

    int arity;

    public:
    void feedforward(real_t *a, real_t *z) {

        int stride_dv = arity;
        int stride_du = arity * in_height;

        for (int y = 0; y < arity; y++) {
            for (int u = 0; u < out_width; u++) {
                for (int v = 0; v < out_height; v++) {

                    int a_ix = y + arity * pool_size * (v + in_height * u);
                    real_t currmax = a[a_ix];

                    for (int du = 0; du < pool_size; du++) {
                        for (int dv = 0; dv < pool_size; dv++) {
                            real_t newvalue = a[a_ix + du * stride_du + dv * stride_dv];
                            currmax = (newvalue > currmax) ? newvalue : currmax;
                        }
                    }

                    z[y + arity * (v + out_height * u)] = currmax;
                }
            }
        }

    }

    void backprop(real_t *a, real_t *dC_dz, real_t *dC_da) {

        int stride_dv = arity;
        int stride_du = arity * in_height;

        int a_size = arity * in_width * in_height;

        for (int i = 0; i < a_size; i++) {
            dC_da[i] = 0.0;
        }

        for (int y = 0; y < arity; y++) {
            for (int u = 0; u < out_width; u++) {
                for (int v = 0; v < out_height; v++) {
                    int argmax_du = 0;
                    int argmax_dv = 0;

                    int a_ix = y + arity * pool_size * (v + in_height * u);
                    real_t currmax = a[a_ix];

                    for (int du = 0; du < pool_size; du++) {
                        for (int dv = 0; dv < pool_size; dv++) {
                            real_t newvalue = a[a_ix + du * stride_du + dv * stride_dv];
                            bool recordholder = (newvalue > currmax);
                            currmax = recordholder ? newvalue : currmax;
                            argmax_du = recordholder ? du : argmax_du;
                            argmax_dv = recordholder ? dv : argmax_dv;
                        }
                    }

                    int z_ix = y + arity * (v + out_height * u);
                    dC_da[a_ix + argmax_du * stride_du + argmax_dv * stride_dv] = dC_dz[z_ix];
                }
            }
        }

    }

    MaxPool(int ow, int oh, int ps, int ar) {

        out_width = ow;
        out_height = oh;
        in_width = ow * ps;
        in_height = oh * ps;
        pool_size = ps;
        arity = ar;

    }

};


class Convolutional : public ILayer {

    int in_arity;
    int out_arity;

    int in_width;
    int out_width;
    int kernel_width;

    int in_height;
    int out_height;
    int kernel_height;

    real_t *biases;
    real_t *weights;

    real_t learn_rate;

    public:
    void feedforward(real_t *a, real_t *z) {

        // Initialise output with biases:
        int z_size = out_width * out_height * out_arity;
        for (int uvy = 0; uvy < z_size; uvy += out_arity) {
            for (int y = 0; y < out_arity; y++) {
                z[y + uvy] = biases[y];
            }
        }

        // Convolve weights with input:
        for (int uprime = 0; uprime < out_width; uprime++) {
            for (int vprime = 0; vprime < out_height; vprime++) {
                for (int du = 0; du < kernel_width; du++) {
                    for (int dv = 0; dv < kernel_height; dv++) {
                        int u = uprime + du;
                        int v = vprime + dv;

                        int w_ix = in_arity * out_arity * (dv + kernel_height * du);
                        int a_min = in_arity * (v + in_height * u);
                        int a_max = a_min + in_arity;
                        int z_min = out_arity * (vprime + out_height * uprime);
                        int z_max = z_min + out_arity;

                        // Come on, gcc, this looks like a nice vectorisable inner loop:
                        for (int z_ix = z_min; z_ix < z_max; z_ix++) {
                            for (int a_ix = a_min; a_ix < a_max; a_ix++) {
                                z[z_ix] += a[a_ix] * weights[w_ix++];
                            }
                        }
                    }
                }
            }
        }

    }

    void backprop(real_t *a, real_t *dC_dz, real_t *dC_da) {

        // Initialise dC_da with zero:
        int a_size = in_width * in_height * in_arity;
        for (int a_ix = 0; a_ix < a_size; a_ix++) {
            dC_da[a_ix] = 0.0;
        }

        // Convolve weights with dC_dz to obtain dC_da:
        for (int uprime = 0; uprime < out_width; uprime++) {
            for (int vprime = 0; vprime < out_height; vprime++) {
                for (int du = 0; du < kernel_width; du++) {
                    for (int dv = 0; dv < kernel_height; dv++) {
                        int u = uprime + du;
                        int v = vprime + dv;

                        int w_ix = in_arity * out_arity * (dv + kernel_height * du);
                        int a_min = in_arity * (v + in_height * u);
                        int a_max = a_min + in_arity;
                        int z_min = out_arity * (vprime + out_height * uprime);
                        int z_max = z_min + out_arity;

                        for (int z_ix = z_min; z_ix < z_max; z_ix++) {
                            for (int a_ix = a_min; a_ix < a_max; a_ix++) {
                                dC_da[a_ix] += dC_dz[z_ix] * weights[w_ix];
                                weights[w_ix++] -= learn_rate * dC_dz[z_ix] * a[a_ix];
                            }
                        }
                    }
                }
            }
        }

        // Initialise output with biases:
        int z_size = out_width * out_height * out_arity;
        for (int uvy = 0; uvy < z_size; uvy += out_arity) {
            for (int y = 0; y < out_arity; y++) {
                biases[y] -= (learn_rate * dC_dz[y + uvy]);
            }
        }
    }

    Convolutional(int iw, int ih, int ia, int ow, int oh, int oa, real_t lr) {

        in_arity = ia;
        in_width = iw;
        in_height = ih;

        out_arity = oa;
        out_width = ow;
        out_height = oh;

        kernel_width = 1 + iw - ow;
        kernel_height = 1 + ih - oh;

        biases = new real_t[out_arity];
        boxmuller(biases, out_arity, 1.0);

        int num_weights = kernel_width * kernel_height * in_arity * out_arity;
        weights = new real_t[num_weights];
        boxmuller(weights, num_weights, sqrt(1.0 / (in_arity * kernel_width * kernel_height)));

        learn_rate = lr;

    }

    ~Convolutional() {

        delete[] biases;
        delete[] weights;

    }

};


class CompleteBipartite : public ILayer {

    int in_arity;
    int out_arity;

    real_t *biases;
    real_t *weights;

    real_t L2_decay;
    real_t learn_rate;

    public:
    void feedforward(real_t *a, real_t *z) {

        for (int i = 0; i < out_arity; i++) {
            z[i] = biases[i];
        }

        for (int j = 0; j < in_arity; j++) {
            for (int i = 0; i < out_arity; i++) {
                z[i] += (weights[j * out_arity + i] * a[j]);
            }
        }

    }

    void backprop(real_t *a, real_t *dC_dz, real_t *dC_da) {

        for (int j = 0; j < in_arity; j++) {
            dC_da[j] = 0.0;
        }

        for (int j = 0; j < in_arity; j++) {
            for (int i = 0; i < out_arity; i++) {
                dC_da[j] += (weights[j * out_arity + i] * dC_dz[i]);
                weights[j * out_arity + i] -= (learn_rate * dC_dz[i] * a[j]);
                weights[j * out_arity + i] *= L2_decay;
            }
        }

        for (int i = 0; i < out_arity; i++) {
            biases[i] -= (learn_rate * dC_dz[i]);
        }
    }

    CompleteBipartite(int ia, int oa, real_t lr, real_t l2d) {

        in_arity = ia;
        out_arity = oa;
        biases = new real_t[out_arity];
        boxmuller(biases, out_arity, 1.0);
        weights = new real_t[out_arity * in_arity];
        boxmuller(weights, out_arity * in_arity, sqrt(1.0 / in_arity));
        L2_decay = l2d;
        learn_rate = lr;

    }

    ~CompleteBipartite() {

        delete[] biases;
        delete[] weights;

    }

};


class Network {

    public:
    std::vector<ILayer*> layers;
    std::vector<int> datasizes;


    int measure_success(mnist_digit *test_data, int test_size) {

        int matches = 0;
        int l = datasizes.size();

        std::vector<real_t*> a;

        for (int k = 0; k < l; k++) {

            a.push_back(new real_t[datasizes[k]]);

        }

        for (int i = 0; i < test_size; i++) {

            test_data[i].realise(a[0]);

            for (int k = 0; k < l-1; k++) {
                layers[k]->feedforward(a[k], a[k+1]);
            }

            real_t maxsat = -2.0;
            int argmax = -1;

            for (int j = 0; j < datasizes[l-1]; j++) { if (a[l-1][j] > maxsat) { argmax = j; maxsat = a[l-1][j];} }

            if (argmax == test_data[i].actual_digit) { matches += 1; }

        }

        for (int k = 0; k < l; k++) {

            delete[] a[k];

        }

        return matches;
    }

    void train_network(mnist_digit *training_data, int training_size) {

        int l = datasizes.size();

        std::vector<real_t*> a;
        std::vector<real_t*> nabla;

        for (int k = 0; k < l; k++) {

            a.push_back(new real_t[datasizes[k]]);
            nabla.push_back(new real_t[datasizes[k]]);

        }

        real_t* target = new real_t[datasizes[l-1]];

        for (int i = 0; i < training_size; i++) {

            training_data[i].realise(a[0]);

            for (int k = 0; k < l-1; k++) {
                layers[k]->feedforward(a[k], a[k+1]);
            }

            training_data[i].realise_target(target);
            for (int j = 0; j < datasizes[l-1]; j++) { nabla[l-2][j] = a[l-1][j] - target[j]; }

            for (int k = l-3; k >= 0; k--) {
                layers[k]->backprop(a[k], nabla[k+1], nabla[k]);
            }

        }

        delete[] target;
        real_t maxnabla = 0.0;
        real_t maxa = 0.0;

        for (int k = 0; k < l; k++) {
            for (int i = 0; i < datasizes[k]; i++) {
                real_t newnabla = fabs(nabla[k][i]);
                if (newnabla > maxnabla) { maxnabla = newnabla; }
                real_t newa = fabs(a[k][i]);
                if (newa > maxa) { maxa = newa; }
            }

            delete[] a[k];
            delete[] nabla[k];

        }

        // std::cout << "Max nabla: " << maxnabla << std::endl;
        // std::cout << "Max a: " << maxa << std::endl;

    }

};


void load_data(mnist_digit *arr, const char* filename, int count) {

    std::ifstream infile;
    infile.open(filename);

    for (int i = 0; i < count; i++) {
        arr[i].populate_from_file(infile);
    }

    infile.close();

}

int main() {

    std::cout << "Loading training data..." << std::endl;
    mnist_digit *training_data = new mnist_digit[50000];
    load_data(training_data, "../data/training_data.in", 50000);

    std::cout << "Loading validation data..." << std::endl;
    mnist_digit *validation_data = new mnist_digit[10000];
    load_data(validation_data, "../data/validation_data.in", 10000);

    std::cout << "Loading test data..." << std::endl;
    mnist_digit *test_data = new mnist_digit[10000];
    load_data(test_data, "../data/test_data.in", 10000);

    std::cout << "Building neural network..." << std::endl;
    
    Network mynet;

    mynet.datasizes.push_back(625);

    mynet.layers.push_back(new Convolutional(25, 25, 1, 20, 20, 5, 0.0005));
    mynet.datasizes.push_back(2000);
    mynet.layers.push_back(new MaxPool(10, 10, 2, 5));
    mynet.datasizes.push_back(500);
    mynet.layers.push_back(new BabbageSigmoid(500));
    mynet.datasizes.push_back(500);
    mynet.layers.push_back(new Convolutional(10, 10, 5, 8, 8, 10, 0.0005));
    mynet.datasizes.push_back(640);
    mynet.layers.push_back(new MaxPool(4, 4, 2, 10));
    mynet.datasizes.push_back(160);
    mynet.layers.push_back(new BabbageSigmoid(160));
    mynet.datasizes.push_back(160);
    mynet.layers.push_back(new CompleteBipartite(160, 10, 0.002, 0.999999));
    mynet.datasizes.push_back(10);
    mynet.layers.push_back(new BabbageSigmoid(10));
    mynet.datasizes.push_back(10);

    std::cout << "Training network (" << training_samples << " training images per epoch)..." << std::endl;

    for (int k = 0; k < 1000; k++) {

        mynet.train_network(training_data, training_samples);
        int matches = mynet.measure_success(test_data, testing_samples);

        std::cout << "Epoch " << (k + 1) << ": " << matches << " agreements." << std::endl;

    }

    return 0;

}
