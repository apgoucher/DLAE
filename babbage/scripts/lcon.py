#!/usr/bin/python

from math import sqrt
import sys



def _argmax(g, start_pooled, start_unpooled):

    for i in xrange(2):
        g.write('R002 = R'+str(start_pooled + i)+'\n')
        g.write('jsrn _unpack_floats\n')
        g.write('memcpy R'+str(start_unpooled + 5*i)+', R003, 5\n')
    g.write('N003 0\n')
    g.write('R002 = R'+str(start_unpooled)+'\n')
    for k in xrange(1, 10):
        g.write('R001 = R002 - R'+str(start_unpooled + k)+'\n')
        g.write('-\nL000\nL001\nCF?5\n')
        g.write('+\nL000\nL'+str(start_unpooled + k)+'\nS002\n')
        g.write('N003 '+str(k)+'\n')
    g.write('R'+str(start_unpooled + 10)+' = R003\n')


def evaluate():

    layername = sys.argv[2]
    outfile = sys.argv[3]
    start_in = int(sys.argv[4])
    start_out = int(sys.argv[5])
    start_scratch = int(sys.argv[6])

    with open(outfile, 'w') as g:

        g.write('.'+layername+'_evaluate:\n\n')

        _argmax(g, start_in, start_scratch)
        _argmax(g, start_out, start_scratch + 11)
        for i in xrange(11):
            g.write('R'+str(start_scratch + i)+' = R'+str(start_scratch + i)+' - R'+str(start_scratch + 11 + i)+'\n')

        for i in xrange(2):
            g.write('memcpy R003, R'+str(start_scratch + 5*i)+', 5\n')
            g.write('jsrn _pack_floats\n')
            g.write('R'+str(start_in + i)+' = R002\n')

        g.write('+\n')
        g.write('L000\n')
        g.write('L'+str(start_scratch + 10)+'\n')
        g.write('cjmp '+layername+'_bad\n')
        g.write('-\n')
        g.write('L000\n')
        g.write('L'+str(start_scratch + 10)+'\n')
        g.write('cjmp '+layername+'_bad\n')
        g.write('jmp '+layername+'_good\n')

        g.write('\n.'+layername+'_bad:\n')
        g.write('A write annotation .\n')
        g.write('ret\n')

        g.write('\n.'+layername+'_good:\n')
        g.write('A write annotation *\n')
        g.write('R030 = R030 + 1\n')
        g.write('R031 = R031 + 1\n')
        g.write('ret\n')


def printreg(g, i):

    x = str(i + 1000)[1:]
    g.write('A write annotation N'+x+' \n')
    g.write('+\nL000\nL'+x+'\nP\n')
    g.write('A write new line\n')

def printreg2(g, first, length):

    g.write('A write in columns\n')
    g.write('A write numbers as 9999999999 9999999999 9999999999 9999999999 9999999999\n')
    for i in xrange(length):
        printreg(g, first + i)
    g.write('A write in rows\n')
    g.write('A write numbers as 9\n')

def hiddenconv():

    layername = sys.argv[2]
    outfile = sys.argv[3]
    start_in = int(sys.argv[4])
    start_out = int(sys.argv[5])
    start_scratch = int(sys.argv[6])
    learn_rate = float(sys.argv[7])

    in_arity = 5
    in_width = 10

    out_arity = 10
    pool_size = 2
    pooled_width = 4

    # learn_rate = 0.0005

    pp_width = pooled_width * pool_size
    lrf_width = in_width - pp_width + pool_size
    kernel_width = in_width - pp_width + 1

    start_weights = start_in + in_width * in_width * (in_arity / 5)
    start_biases = start_weights + kernel_width * kernel_width * (in_arity * out_arity / 5)
    start_out_argmax = start_biases + (out_arity / 5)

    start_lrf = start_scratch
    start_unpooled = start_lrf + lrf_width * lrf_width * (in_arity / 5)
    start_pooled = start_unpooled + (out_arity * pool_size * pool_size)
    start_packed = start_pooled + out_arity
    row_argmax = start_packed + (out_arity / 5)

    with open(outfile, 'w') as g:

        g.write('.'+layername+'_debug:\n\n')

        g.write('A write annotation \033[33;1m***** Debug data for layer '+layername+' *****\033[0m\n')
        g.write('A write annotation \033[31;1mInputs:\033[0m\n')
        printreg2(g, start_in, in_width * in_width * (in_arity / 5))
        g.write('A write annotation \033[31;1mWeights:\033[0m\n')
        printreg2(g, start_weights, kernel_width * kernel_width * (in_arity * out_arity / 5))
        g.write('A write annotation \033[31;1mBiases:\033[0m\n')
        printreg2(g, start_biases, out_arity / 5)
        g.write('A write annotation \033[31;1mArgmaxes:\033[0m\n')
        printreg2(g, start_out_argmax, pooled_width)
        g.write('A write annotation \033[31;1mOutputs:\033[0m\n')
        printreg2(g, start_out, pooled_width * pooled_width * (out_arity / 5))

        g.write('ret\n\n')

        g.write('.'+layername+'_init:\n\n')

        g.write('N996 100000000\n')
        for j in xrange(out_arity/5):
            g.write('jsr __random_packed\n')
            g.write('R'+str(start_biases + j)+' = R002\n')

        g.write('N996 '+str(int(100000000 * sqrt(1.0 / (kernel_width * kernel_width * in_arity))))+'\n')
        for k in xrange((kernel_width * kernel_width * out_arity * in_arity)/5):
            g.write('jsr __random_packed\n')
            g.write('R'+str(start_weights + k)+' = R002\n')

        g.write('ret\n\n')


        g.write('.'+layername+'_feedforward:\n\n')

        for i in xrange(pooled_width):

            # Zero the row argmax:
            g.write('N'+str(row_argmax)+' 0\n')

            for j in xrange(pooled_width):

                # Copy LRF from input area:
                for u in xrange(lrf_width):
                    for v in xrange(lrf_width):
                        uprime = u + i * pool_size
                        vprime = v + j * pool_size
                        globreg = 'R'+str(start_in + (in_arity / 5) * (vprime + uprime * in_width))
                        locareg = 'R'+str(start_lrf + (in_arity / 5) * (v + u * lrf_width))
                        g.write('memcpy '+locareg+', '+globreg+', '+str(in_arity / 5)+'\n')

                # Do the convolution for one output cell:
                g.write('jsr '+layername+'_feedforward_cell\n')

                # Copy result to output area:
                globreg = 'R'+str(start_out + (out_arity / 5) * (i * pooled_width + j))
                locareg = 'R'+str(start_packed)
                g.write('memcpy '+globreg+', '+locareg+', '+str(out_arity / 5)+'\n')

            # Save the row argmax:
            g.write('R'+str(start_out_argmax + i)+' = R'+str(row_argmax)+'\n')

        g.write('ret\n\n')


        g.write('.'+layername+'_feedforward_cell:\n\n')

        # Initialise unpooled output with biases:
        for j in xrange(out_arity/5):
            g.write('R002 = R'+str(start_biases + j)+'\n')
            g.write('jsrn _unpack_floats\n')
            for i in xrange(pool_size * pool_size):
                g.write('memcpy R'+str(start_unpooled + j * 5 + i * out_arity)+', R003, 5\n')

        # Convolve:
        for u in xrange(kernel_width):
            for v in xrange(kernel_width):
                for j in xrange(out_arity/5):
                    for i in xrange(in_arity/5):
                        wstride = out_arity / 5
                        xstride = in_arity / 5

                        # Retrieve weights:
                        smatrix = start_weights + (out_arity * in_arity / 5) * (v + u * kernel_width)
                        g.write('R986 = R'+str(smatrix + j + wstride * (5 * i))+'\n')
                        g.write('R987 = R'+str(smatrix + j + wstride * (5 * i + 1))+'\n')
                        g.write('R988 = R'+str(smatrix + j + wstride * (5 * i + 2))+'\n')
                        g.write('R989 = R'+str(smatrix + j + wstride * (5 * i + 3))+'\n')
                        g.write('R990 = R'+str(smatrix + j + wstride * (5 * i + 4))+'\n')
                        g.write('jsr __unpack_b\n')

                        for du in xrange(pool_size):
                            for dv in xrange(pool_size):
                                uprime = u + du
                                vprime = v + dv
                                g.write('R985 = R'+str(start_lrf + i + xstride * (vprime + uprime * lrf_width))+'\n')
                                g.write('jsr __unpack_a\n')
                                meh = 'R'+str(start_unpooled + 5*j + out_arity * (dv + pool_size * du))
                                g.write('memcpy R980, '+meh+', 5\n')
                                g.write('jsrn _c_pluseq_ab\n')
                                g.write('memcpy '+meh+', R980, 5\n')

        # Pool:
        for j in xrange(out_arity):
            g.write('N003 0\n')
            g.write('R002 = R'+str(start_unpooled + j)+'\n')
            for k in xrange(1, pool_size * pool_size):
                g.write('R001 = R002 - R'+str(start_unpooled + j + k * out_arity)+'\n')
                g.write('-\nL000\nL001\nCF?5\n')
                g.write('+\nL000\nL'+str(start_unpooled + j + k * out_arity)+'\nS002\n')
                g.write('N003 '+str(k * (10 ** (out_arity * pooled_width - 1)))+'\n')
            # Now R003 contains argmax and R002 contains max:
            g.write('R'+str(start_pooled + j)+' = R002\n')
            g.write('R'+str(row_argmax)+' = R'+str(row_argmax)+' / 10\n')
            g.write('R'+str(row_argmax)+' = R'+str(row_argmax)+' + R003\n')

        # Pack:
        for j in xrange(out_arity/5):
            g.write('memcpy R003, R'+str(start_pooled + 5*j)+', 5\n')
            g.write('jsrn _pack_floats\n')
            g.write('R'+str(start_packed + j)+' = R002\n')

        g.write('ret\n\n')


        g.write('.'+layername+'_backprop:\n\n')

        g.write('jsr '+layername+'_updateweights\n')

        # Zero the input array:
        for i in xrange(in_width * in_width * (in_arity / 5)):
            g.write('N'+str(start_in + i)+' '+('5000000000'*5)+'\n')

        for i in xrange(pooled_width):

            # Save the row argmax:
            g.write('R'+str(row_argmax)+' = R'+str(start_out_argmax + i)+'\n')

            for j in xrange(pooled_width):

                # Copy derivative from output area:
                globreg = 'R'+str(start_out + (out_arity / 5) * (i * pooled_width + j))
                locareg = 'R'+str(start_packed)
                g.write('memcpy '+locareg+', '+globreg+', '+str(out_arity / 5)+'\n')

                # Copy LRF from input area:
                for u in xrange(lrf_width):
                    for v in xrange(lrf_width):
                        uprime = u + i * pool_size
                        vprime = v + j * pool_size
                        globreg = 'R'+str(start_in + (in_arity / 5) * (vprime + uprime * in_width))
                        locareg = 'R'+str(start_lrf + (in_arity / 5) * (v + u * lrf_width))
                        g.write('memcpy '+locareg+', '+globreg+', '+str(in_arity / 5)+'\n')

                # Do the nvolution for one output cell:
                g.write('jsr '+layername+'_backprop_cell\n')

                # Return LRF to input area:
                for u in xrange(lrf_width):
                    for v in xrange(lrf_width):
                        uprime = u + i * pool_size
                        vprime = v + j * pool_size
                        globreg = 'R'+str(start_in + (in_arity / 5) * (vprime + uprime * in_width))
                        locareg = 'R'+str(start_lrf + (in_arity / 5) * (v + u * lrf_width))
                        g.write('memcpy '+globreg+', '+locareg+', '+str(in_arity / 5)+'\n')

        g.write('ret\n\n')


        g.write('.'+layername+'_backprop_cell:\n\n')

        # Unpack derivatives:
        for j in xrange(out_arity/5):
            g.write('R002 = R'+str(start_packed + j)+'\n')
            # g.write('P\n')
            g.write('jsrn _unpack_floats\n')
            g.write('memcpy R'+str(start_pooled + 5*j)+', R003, 5\n')
            # g.write('P\n')

        # Unpool derivatives:
        for j in xrange(out_arity):

            # Zero existing data:
            for k in xrange(pool_size * pool_size):
                g.write('N'+str(start_unpooled + j + k * out_arity)+' 0\n')

            # Extract max and argmax:
            g.write('R002 = R'+str(start_pooled + j)+'\n')
            g.write('N001 10\n')
            g.write('vrshift R003, R'+str(row_argmax)+', 1\n')
            g.write('N001 1\n')

            # Now R003 contains argmax and R002 contains max:
            for k in xrange(pool_size * pool_size):
                g.write('-\nL003\nL001\nS003\n')
                g.write('CF?'+str(pool_size * pool_size * 5 - 1)+'\n')
            g.write('A write in columns\n')
            g.write('A write annotation Invalid argmax\n')
            g.write('B\n')
            g.write('H fatal error\n')
            for k in xrange(pool_size * pool_size):
                g.write('+\nL000\nL002\n')
                g.write('S'+str(start_unpooled + j + k * out_arity)+'\n')
                g.write('CF+'+str((pool_size * pool_size - (k + 1)) * 5 + 1)+'\n')
            g.write('A write annotation (unreachable code)\n')

        # Nvolve:
        for u in xrange(kernel_width):
            for v in xrange(kernel_width):
                for j in xrange(out_arity/5):
                    for i in xrange(in_arity/5):
                        wstride = out_arity / 5
                        xstride = in_arity / 5

                        # Retrieve weights:
                        smatrix = start_weights + (out_arity * in_arity / 5) * (v + u * kernel_width)
                        g.write('R986 = R'+str(smatrix + j + wstride * (5 * i))+'\n')
                        g.write('R987 = R'+str(smatrix + j + wstride * (5 * i + 1))+'\n')
                        g.write('R988 = R'+str(smatrix + j + wstride * (5 * i + 2))+'\n')
                        g.write('R989 = R'+str(smatrix + j + wstride * (5 * i + 3))+'\n')
                        g.write('R990 = R'+str(smatrix + j + wstride * (5 * i + 4))+'\n')
                        g.write('jsr __unpack_b\n')

                        for du in xrange(pool_size):
                            for dv in xrange(pool_size):
                                uprime = u + du
                                vprime = v + dv
                                g.write('R985 = R'+str(start_lrf + i + xstride * (vprime + uprime * lrf_width))+'\n')
                                g.write('jsr __unpack_a\n')
                                meh = 'R'+str(start_unpooled + 5*j + out_arity * (dv + pool_size * du))
                                g.write('memcpy R980, '+meh+', 5\n')
                                # g.write('N002 -'+str(int((10 ** 20) * learn_rate))+'\n')
                                # g.write('jsrn _scale_c\n')
                                g.write('jsrn _a_pluseq_bc\n')
                                g.write('jsr __pack_a\n')
                                g.write('R'+str(start_lrf + i + xstride * (vprime + uprime * lrf_width))+' = R985\n')

        g.write('ret\n\n')


        g.write('.'+layername+'_updateweights:\n\n')

        for i in xrange(pooled_width):

            # Save the row argmax:
            g.write('R'+str(row_argmax)+' = R'+str(start_out_argmax + i)+'\n')

            for j in xrange(pooled_width):

                # Copy derivative from output area:
                globreg = 'R'+str(start_out + (out_arity / 5) * (i * pooled_width + j))
                locareg = 'R'+str(start_packed)
                g.write('memcpy '+locareg+', '+globreg+', '+str(out_arity / 5)+'\n')

                # Copy LRF from input area:
                for u in xrange(lrf_width):
                    for v in xrange(lrf_width):
                        uprime = u + i * pool_size
                        vprime = v + j * pool_size
                        globreg = 'R'+str(start_in + (in_arity / 5) * (vprime + uprime * in_width))
                        locareg = 'R'+str(start_lrf + (in_arity / 5) * (v + u * lrf_width))
                        g.write('memcpy '+locareg+', '+globreg+', '+str(in_arity / 5)+'\n')

                # Do the nvolution for one output cell:
                g.write('jsr '+layername+'_updateweights_cell\n')

        g.write('ret\n\n')


        g.write('.'+layername+'_updateweights_cell:\n\n')

        # Unpack derivatives:
        for j in xrange(out_arity/5):
            g.write('R002 = R'+str(start_packed + j)+'\n')
            # g.write('P\n')
            g.write('jsrn _unpack_floats\n')
            g.write('memcpy R'+str(start_pooled + 5*j)+', R003, 5\n')
            # g.write('P\n')

        # Unpool derivatives:
        for j in xrange(out_arity):

            # Zero existing data:
            for k in xrange(pool_size * pool_size):
                g.write('N'+str(start_unpooled + j + k * out_arity)+' 0\n')

            # Extract max and argmax:
            g.write('R002 = R'+str(start_pooled + j)+'\n')
            g.write('N001 10\n')
            g.write('vrshift R003, R'+str(row_argmax)+', 1\n')
            g.write('N001 1\n')

            # Now R003 contains argmax and R002 contains max:
            for k in xrange(pool_size * pool_size):
                g.write('-\nL003\nL001\nS003\n')
                g.write('CF?'+str(pool_size * pool_size * 5 - 1)+'\n')
            g.write('A write in columns\n')
            g.write('A write annotation Invalid argmax\n')
            g.write('B\n')
            g.write('H fatal error\n')
            for k in xrange(pool_size * pool_size):
                g.write('+\nL000\nL002\n')
                g.write('S'+str(start_unpooled + j + k * out_arity)+'\n')
                g.write('CF+'+str((pool_size * pool_size - (k + 1)) * 5 + 1)+'\n')
            g.write('A write annotation (unreachable code)\n')


        # Nvolve:
        for u in xrange(kernel_width):
            for v in xrange(kernel_width):
                for j in xrange(out_arity/5):
                    for i in xrange(in_arity/5):
                        wstride = out_arity / 5
                        xstride = in_arity / 5

                        # Retrieve weights:
                        smatrix = start_weights + (out_arity * in_arity / 5) * (v + u * kernel_width)
                        g.write('R986 = R'+str(smatrix + j + wstride * (5 * i))+'\n')
                        g.write('R987 = R'+str(smatrix + j + wstride * (5 * i + 1))+'\n')
                        g.write('R988 = R'+str(smatrix + j + wstride * (5 * i + 2))+'\n')
                        g.write('R989 = R'+str(smatrix + j + wstride * (5 * i + 3))+'\n')
                        g.write('R990 = R'+str(smatrix + j + wstride * (5 * i + 4))+'\n')
                        g.write('jsr __unpack_b\n')

                        for du in xrange(pool_size):
                            for dv in xrange(pool_size):
                                uprime = u + du
                                vprime = v + dv
                                g.write('R985 = R'+str(start_lrf + i + xstride * (vprime + uprime * lrf_width))+'\n')
                                g.write('jsr __unpack_a\n')
                                g.write('N002 -'+str(int((10 ** 20) * learn_rate))+'\n')
                                g.write('jsrn _scale_a\n')
                                meh = 'R'+str(start_unpooled + 5*j + out_arity * (dv + pool_size * du))
                                g.write('memcpy R980, '+meh+', 5\n')
                                g.write('jsrn _b_pluseq_ac\n')

                        # Replace weights:
                        g.write('jsr __pack_b\n')
                        g.write('R'+str(smatrix + j + wstride * (5 * i))+' = R986\n')
                        g.write('R'+str(smatrix + j + wstride * (5 * i + 1))+' = R987\n')
                        g.write('R'+str(smatrix + j + wstride * (5 * i + 2))+' = R988\n')
                        g.write('R'+str(smatrix + j + wstride * (5 * i + 3))+' = R989\n')
                        g.write('R'+str(smatrix + j + wstride * (5 * i + 4))+' = R990\n')

        # Update biases:
        for j in xrange(out_arity/5):
            g.write('R002 = R'+str(start_biases + j)+'\n')
            g.write('jsrn _unpack_floats\n')
            for k in xrange(pool_size * pool_size):
                for i in xrange(5):
                    # g.write('N001 0\n')
                    # g.write('N001 -20000000000000000\n')
                    g.write('N001 -'+str(int((10 ** 20) * learn_rate))+'\n')
                    g.write('*\n')
                    g.write('L001\n')
                    g.write('L'+str(start_unpooled + j * 5 + k * out_arity + i)+'\n')
                    g.write('>20\n')
                    g.write('S001\n')
                    # g.write('P\n')
                    meh = 'R00' + str(3 + i)
                    g.write(meh + ' = ' + meh + ' + R001\n')
                    # g.write('P\n')
            g.write('jsrn _pack_floats\n')
            g.write('R'+str(start_biases + j)+' = R002\n')

        g.write('ret\n\n')


def inputconv():

    layername = sys.argv[2]
    outfile = sys.argv[3]
    start_in = int(sys.argv[4])
    start_out = int(sys.argv[5])
    start_scratch = int(sys.argv[6])
    learn_rate = float(sys.argv[7])

    out_arity = 5
    pool_size = 2
    pooled_width = 10

    pp_width = pooled_width * pool_size
    lrf_width = 25 - pp_width + pool_size
    kernel_width = 25 - pp_width + 1

    start_weights = start_in + 25
    start_biases = start_weights + kernel_width * kernel_width * (out_arity / 5)
    start_out_argmax = start_biases + (out_arity / 5)

    start_strip = start_scratch + lrf_width
    start_lrf = start_strip + lrf_width
    start_unpooled = start_lrf + (lrf_width * lrf_width)
    start_pooled = start_unpooled + (out_arity * pool_size * pool_size)
    start_packed = start_pooled + out_arity
    start_collector = start_packed + (out_arity / 5)
    row_argmax = start_collector + pooled_width * (out_arity / 5)

    with open(outfile, 'w') as g:

        g.write('.'+layername+'_debug:\n\n')

        g.write('A write annotation \033[33;1m***** Debug data for layer '+layername+' *****\033[0m\n')
        g.write('A write annotation \033[31;1mInputs:\033[0m\n')
        printreg2(g, start_in, 25)
        g.write('A write annotation \033[31;1mWeights:\033[0m\n')
        printreg2(g, start_weights, kernel_width * kernel_width * (out_arity / 5))
        g.write('A write annotation \033[31;1mBiases:\033[0m\n')
        printreg2(g, start_biases, out_arity / 5)
        g.write('A write annotation \033[31;1mArgmaxes:\033[0m\n')
        printreg2(g, start_out_argmax, pooled_width)
        g.write('A write annotation \033[31;1mOutputs:\033[0m\n')
        printreg2(g, start_out, pooled_width * pooled_width * (out_arity / 5))

        g.write('ret\n\n')

        g.write('.'+layername+'_init:\n\n')

        g.write('N996 100000000\n')
        for j in xrange(out_arity/5):
            g.write('jsr __random_packed\n')
            g.write('R'+str(start_biases + j)+' = R002\n')

        g.write('N996 '+str(int(100000000 * (1.0 / kernel_width)))+'\n')
        for k in xrange((kernel_width * kernel_width * out_arity)/5):
            g.write('jsr __random_packed\n')
            g.write('R'+str(start_weights + k)+' = R002\n')

        g.write('ret\n\n')


        g.write('.'+layername+'_feedforward:\n\n')

        for j in xrange(pooled_width):
            g.write('memcpy R'+str(start_scratch)+', R'+str(start_in + pool_size * j)+', '+str(lrf_width)+'\n')
            g.write('jsr '+layername+'_feedforward_row\n')
            g.write('R'+str(start_out_argmax + j)+' = R'+str(row_argmax)+'\n')
            r_global = 'R'+str(start_out + j * pooled_width * (out_arity / 5))
            g.write('memcpy '+r_global+', R'+str(start_collector)+', '+str(pooled_width * (out_arity / 5))+'\n')

        g.write('ret\n\n')

        g.write('.'+layername+'_feedforward_row:\n\n')

        # Zero the row argmax:
        g.write('N'+str(row_argmax)+' 0\n')

        for j in xrange(pooled_width):
            g.write('memcpy R'+str(start_strip)+', R'+str(start_scratch)+', '+str(lrf_width)+'\n')
            g.write('jsr '+layername+'_feedforward_cell\n')
            g.write('N001 '+str(100 ** pool_size)+'\n')
            g.write('vrshift R'+str(start_strip)+', R'+str(start_scratch)+', '+str(lrf_width)+'\n')
            g.write('memcpy R'+str(start_collector + j * (out_arity / 5))+', R'+str(start_packed)+', '+str(out_arity / 5)+'\n')

        g.write('ret\n\n')

        g.write('.'+layername+'_feedforward_cell:\n\n')

        # Unpack local receptive field:
        for j in xrange(lrf_width):
            g.write('N001 100\n')
            g.write('vrshift R'+str(start_lrf + lrf_width * j)+', R'+str(start_strip)+', '+str(lrf_width)+'\n')
            for i in xrange(lrf_width):
                meh = 'R'+str(start_lrf + lrf_width * j + i)
                g.write(meh+' = '+meh+' * 1000000000000000000\n')

        # Initialise unpooled output with biases:
        for j in xrange(out_arity/5):
            g.write('R002 = R'+str(start_biases + j)+'\n')
            g.write('jsrn _unpack_floats\n')
            for i in xrange(pool_size * pool_size):
                g.write('memcpy R'+str(start_unpooled + j * 5 + i * out_arity)+', R003, 5\n')

        # Convolve:
        for u in xrange(kernel_width):
            for v in xrange(kernel_width):
                for j in xrange(out_arity/5):
                    g.write('R002 = R'+str(start_weights + j + (out_arity/5) * (v + u * kernel_width))+'\n')
                    g.write('jsrn _unpack_floats\n')
                    for du in xrange(pool_size):
                        for dv in xrange(pool_size):
                            uprime = u + du
                            vprime = v + dv
                            for i in xrange(5):
                                g.write('*\n')
                                g.write('L00' + str(3 + i) + '\n')
                                g.write('L'+str(start_lrf + vprime + lrf_width * uprime)+'\n')
                                g.write('>20\n')
                                g.write('S001\n')
                                meh = 'R'+str(start_unpooled + i + 5*j + out_arity * (dv + pool_size * du))
                                g.write(meh + ' = ' + meh + ' + R001\n')

        # Pool:
        for j in xrange(out_arity):
            g.write('N003 0\n')
            g.write('R002 = R'+str(start_unpooled + j)+'\n')
            for k in xrange(1, pool_size * pool_size):
                g.write('R001 = R002 - R'+str(start_unpooled + j + k * out_arity)+'\n')
                g.write('-\nL000\nL001\nCF?5\n')
                g.write('+\nL000\nL'+str(start_unpooled + j + k * out_arity)+'\nS002\n')
                g.write('N003 '+str(k * (10 ** (out_arity * pooled_width - 1)))+'\n')
            # Now R003 contains argmax and R002 contains max:
            g.write('R'+str(start_pooled + j)+' = R002\n')
            g.write('R'+str(row_argmax)+' = R'+str(row_argmax)+' / 10\n')
            g.write('R'+str(row_argmax)+' = R'+str(row_argmax)+' + R003\n')

        # Pack:
        for j in xrange(out_arity/5):
            g.write('memcpy R003, R'+str(start_pooled + 5*j)+', 5\n')
            g.write('jsrn _pack_floats\n')
            g.write('R'+str(start_packed + j)+' = R002\n')

        g.write('ret\n\n')

        g.write('.'+layername+'_backprop:\n\n')

        for j in xrange(pooled_width):
            g.write('memcpy R'+str(start_scratch)+', R'+str(start_in + pool_size * j)+', '+str(lrf_width)+'\n')
            g.write('R'+str(row_argmax)+' = R'+str(start_out_argmax + j)+'\n')
            r_global = 'R'+str(start_out + j * pooled_width * (out_arity / 5))
            g.write('memcpy R'+str(start_collector)+', '+r_global+', '+str(pooled_width * (out_arity / 5))+'\n')
            g.write('jsr '+layername+'_backprop_row\n')

        g.write('ret\n\n')

        g.write('.'+layername+'_backprop_row:\n\n')

        for j in xrange(pooled_width):
            g.write('memcpy R'+str(start_packed)+', R'+str(start_collector + j * (out_arity / 5))+', '+str(out_arity / 5)+'\n')
            # g.write('P\n')
            g.write('memcpy R'+str(start_strip)+', R'+str(start_scratch)+', '+str(lrf_width)+'\n')
            g.write('jsr '+layername+'_backprop_cell\n')
            g.write('N001 '+str(100 ** pool_size)+'\n')
            g.write('vrshift R'+str(start_strip)+', R'+str(start_scratch)+', '+str(lrf_width)+'\n')

        g.write('ret\n\n')

        g.write('.'+layername+'_backprop_cell:\n\n')

        # Unpack local receptive field:
        for j in xrange(lrf_width):
            g.write('N001 100\n')
            g.write('vrshift R'+str(start_lrf + lrf_width * j)+', R'+str(start_strip)+', '+str(lrf_width)+'\n')
            for i in xrange(lrf_width):
                meh = 'R'+str(start_lrf + lrf_width * j + i)
                g.write(meh+' = '+meh+' * 1000000000000000000\n')

        # Unpack derivatives:
        for j in xrange(out_arity/5):
            g.write('R002 = R'+str(start_packed + j)+'\n')
            # g.write('P\n')
            g.write('jsrn _unpack_floats\n')
            g.write('memcpy R'+str(start_pooled + 5*j)+', R003, 5\n')
            # g.write('P\n')

        # Unpool derivatives:
        for j in xrange(out_arity):

            # Zero existing data:
            for k in xrange(pool_size * pool_size):
                g.write('N'+str(start_unpooled + j + k * out_arity)+' 0\n')

            # Extract max and argmax:
            g.write('R002 = R'+str(start_pooled + j)+'\n')
            g.write('N001 10\n')
            g.write('vrshift R003, R'+str(row_argmax)+', 1\n')
            g.write('N001 1\n')

            # Now R003 contains argmax and R002 contains max:
            for k in xrange(pool_size * pool_size):
                g.write('-\nL003\nL001\nS003\n')
                g.write('CF?'+str(pool_size * pool_size * 5 - 1)+'\n')
            g.write('A write in columns\n')
            g.write('A write annotation Invalid argmax\n')
            g.write('B\n')
            g.write('H fatal error\n')
            for k in xrange(pool_size * pool_size):
                g.write('+\nL000\nL002\n')
                g.write('S'+str(start_unpooled + j + k * out_arity)+'\n')
                g.write('CF+'+str((pool_size * pool_size - (k + 1)) * 5 + 1)+'\n')
            g.write('A write annotation (unreachable code)\n')

            # Print partial derivatives:
            #for k in xrange(pool_size * pool_size):
            #    g.write('+\n')
            #    g.write('L'+str(start_unpooled + j + k * out_arity)+'\n')
            #    g.write('L000\n')
            #    g.write('P\n')

        # Update weights by performing a nvolution:
        for u in xrange(kernel_width):
            for v in xrange(kernel_width):
                for j in xrange(out_arity/5):
                    g.write('R002 = R'+str(start_weights + j + (out_arity/5) * (v + u * kernel_width))+'\n')
                    g.write('jsrn _unpack_floats\n')
                    for du in xrange(pool_size):
                        for dv in xrange(pool_size):
                            uprime = u + du
                            vprime = v + dv
                            for i in xrange(5):
                                g.write('N001 -'+str(int((10 ** 20) * learn_rate))+'\n')
                                # g.write('N001 -20000000000000000\n')
                                # g.write('N001 0\n')
                                g.write('*\n')
                                g.write('L001\n')
                                g.write('L'+str(start_lrf + vprime + lrf_width * uprime)+'\n')
                                g.write('>20\n')
                                g.write('S001\n')
                                g.write('*\n')
                                g.write('L001\n')
                                g.write('L'+str(start_unpooled + i + 5*j + out_arity * (dv + pool_size * du))+'\n')
                                g.write('>20\n')
                                g.write('S001\n')
                                meh = 'R00' + str(3 + i)
                                g.write(meh + ' = ' + meh + ' + R001\n')
                    g.write('jsrn _pack_floats\n')
                    g.write('R'+str(start_weights + j + (out_arity/5) * (v + u * kernel_width))+' = R002\n')

        # Update biases:
        for j in xrange(out_arity/5):
            g.write('R002 = R'+str(start_biases + j)+'\n')
            g.write('jsrn _unpack_floats\n')
            for k in xrange(pool_size * pool_size):
                for i in xrange(5):
                    # g.write('N001 0\n')
                    # g.write('N001 -20000000000000000\n')
                    g.write('N001 -'+str(int((10 ** 20) * learn_rate))+'\n')
                    g.write('*\n')
                    g.write('L001\n')
                    g.write('L'+str(start_unpooled + j * 5 + k * out_arity + i)+'\n')
                    g.write('>20\n')
                    g.write('S001\n')
                    # g.write('P\n')
                    meh = 'R00' + str(3 + i)
                    g.write(meh + ' = ' + meh + ' + R001\n')
                    # g.write('P\n')
            g.write('jsrn _pack_floats\n')
            g.write('R'+str(start_biases + j)+' = R002\n')


        g.write('ret\n\n')





def bipartite():

    layername = sys.argv[2]
    outfile = sys.argv[3]
    start_in = int(sys.argv[4])
    start_out = int(sys.argv[5])
    start_scratch = int(sys.argv[6])

    in_arity = int(sys.argv[7])
    out_arity = int(sys.argv[8])

    learn_rate = float(sys.argv[9])

    start_weights = start_in + in_arity / 5
    start_biases = start_weights + (in_arity * out_arity) / 5

    # print('Inputs: '+str(start_in)+' ... '+str(start_in + in_arity/5 - 1))
    # print('Weights: '+str(start_weights)+' ... '+str(start_weights + (in_arity * out_arity)/5 - 1))
    # print('Biases: '+str(start_biases)+' ... '+str(start_biases + out_arity/5 - 1))
    # print('Outputs: '+str(start_out)+' ... '+str(start_out + out_arity/5 - 1))
    wstride = out_arity/5

    with open(outfile, 'w') as g:

        g.write('.'+layername+'_debug:\n\n')

        g.write('A write annotation \033[33;1m***** Debug data for layer '+layername+' *****\033[0m\n')
        g.write('A write annotation \033[31;1mInputs:\033[0m\n')
        printreg2(g, start_in, (in_arity / 5))
        g.write('A write annotation \033[31;1mWeights:\033[0m\n')
        printreg2(g, start_weights, (in_arity * out_arity / 5))
        g.write('A write annotation \033[31;1mBiases:\033[0m\n')
        printreg2(g, start_biases, out_arity / 5)
        g.write('A write annotation \033[31;1mOutputs:\033[0m\n')
        printreg2(g, start_out, (out_arity / 5))

        g.write('ret\n\n')

        g.write('.'+layername+'_init:\n\n')

        g.write('N996 100000000\n')
        for j in xrange(out_arity/5):
            g.write('jsr __random_packed\n')
            g.write('R'+str(start_biases + j)+' = R002\n')

        g.write('N996 '+str(int(100000000 * sqrt(1.0 / in_arity)))+'\n')
        for k in xrange((in_arity * out_arity)/5):
            g.write('jsr __random_packed\n')
            g.write('R'+str(start_weights + k)+' = R002\n')

        g.write('ret\n\n')


        g.write('.'+layername+'_feedforward:\n\n')

        for j in xrange(out_arity/5):
            g.write('R991 = R'+str(start_biases + j)+'\n')
            g.write('jsr __unpack_c\n')

            for i in xrange(in_arity/5):
                g.write('R985 = R'+str(start_in + i)+'\n')
                g.write('jsr __unpack_a\n')
                g.write('R986 = R'+str(start_weights + j + wstride * (5 * i))+'\n')
                g.write('R987 = R'+str(start_weights + j + wstride * (5 * i + 1))+'\n')
                g.write('R988 = R'+str(start_weights + j + wstride * (5 * i + 2))+'\n')
                g.write('R989 = R'+str(start_weights + j + wstride * (5 * i + 3))+'\n')
                g.write('R990 = R'+str(start_weights + j + wstride * (5 * i + 4))+'\n')
                g.write('jsr __unpack_b\n')
                g.write('jsrn _c_pluseq_ab\n')

            g.write('jsr __pack_c\n')
            g.write('R'+str(start_out + j)+' = R991\n\n')

        g.write('ret\n\n')


        g.write('.'+layername+'_backprop:\n\n')

        # g.write('A write annotation Updating weights...\n')

        # Update weights:
        for j in xrange(out_arity/5):
            g.write('R991 = R'+str(start_out + j)+'\n')
            # g.write('A write annotation reading output:\nP\n')
            g.write('jsr __unpack_c\n')
            # g.write('N002 0\n')
            # g.write('N002 -200000000000000000\n')
            g.write('N002 -'+str(int((10 ** 20) * learn_rate))+'\n')
            g.write('jsrn _scale_c\n')

            for i in xrange(in_arity/5):
                # g.write('R001 = R980\nP\n')
                g.write('R985 = R'+str(start_in + i)+'\n')
                g.write('jsr __unpack_a\n')
                g.write('R986 = R'+str(start_weights + j + wstride * (5 * i))+'\n')
                g.write('R987 = R'+str(start_weights + j + wstride * (5 * i + 1))+'\n')
                g.write('R988 = R'+str(start_weights + j + wstride * (5 * i + 2))+'\n')
                g.write('R989 = R'+str(start_weights + j + wstride * (5 * i + 3))+'\n')
                g.write('R990 = R'+str(start_weights + j + wstride * (5 * i + 4))+'\n')
                g.write('jsr __unpack_b\n')
                g.write('jsrn _b_pluseq_ac\n')
                # g.write('R001 = R980\nP\n')
                g.write('N002 99999900000000000000\n')
                g.write('jsrn _scale_b\n')
                g.write('jsr __pack_b\n')
                # g.write('R001 = R980\nP\n')
                g.write('R'+str(start_weights + j + wstride * (5 * i))+' = R986\n')
                g.write('R'+str(start_weights + j + wstride * (5 * i + 1))+' = R987\n')
                g.write('R'+str(start_weights + j + wstride * (5 * i + 2))+' = R988\n')
                g.write('R'+str(start_weights + j + wstride * (5 * i + 3))+' = R989\n')
                g.write('R'+str(start_weights + j + wstride * (5 * i + 4))+' = R990\n')

        # g.write('A write annotation Updating biases...\n')

        # Update biases:
        for j in xrange(out_arity/5):
            g.write('R991 = R'+str(start_out + j)+'\n')
            g.write('jsr __unpack_c\n')
            # g.write('N002 -200000000000000000\n')
            g.write('N002 -'+str(int((10 ** 20) * learn_rate))+'\n')
            g.write('jsrn _scale_c\n')
            g.write('R002 = R'+str(start_biases + j)+'\n')
            g.write('jsrn _unpack_floats\n')
            g.write('R003 = R003 + R980\n')
            g.write('R004 = R004 + R981\n')
            g.write('R005 = R005 + R982\n')
            g.write('R006 = R006 + R983\n')
            g.write('R007 = R007 + R984\n')
            g.write('jsrn _pack_floats\n')
            g.write('R'+str(start_biases + j)+' = R002\n')

        # Backpropagate derivative:
        for i in xrange(in_arity/5):
            g.write('N950 0\n')
            g.write('N951 0\n')
            g.write('N952 0\n')
            g.write('N953 0\n')
            g.write('N954 0\n')

            for j in xrange(out_arity/5):
                g.write('R991 = R'+str(start_out + j)+'\n')
                g.write('jsr __unpack_c\n')
                g.write('R986 = R'+str(start_weights + j + wstride * (5 * i))+'\n')
                g.write('R987 = R'+str(start_weights + j + wstride * (5 * i + 1))+'\n')
                g.write('R988 = R'+str(start_weights + j + wstride * (5 * i + 2))+'\n')
                g.write('R989 = R'+str(start_weights + j + wstride * (5 * i + 3))+'\n')
                g.write('R990 = R'+str(start_weights + j + wstride * (5 * i + 4))+'\n')
                g.write('jsr __unpack_b\n')
                g.write('jsrn _a_pluseq_bc\n')

            g.write('jsr __pack_a\n')
            g.write('R'+str(start_in + i)+' = R985\n')

        g.write('ret\n\n')



def sigmoid(finality):

    layername = sys.argv[2]
    outfile = sys.argv[3]
    start_in = int(sys.argv[4])
    start_out = int(sys.argv[5])
    start_scratch = int(sys.argv[6])

    arity = int(sys.argv[7])

    with open(outfile, 'w') as g:

        g.write('.'+layername+'_debug:\n\n')

        g.write('A write annotation \033[33;1m***** Debug data for layer '+layername+' *****\033[0m\n')
        g.write('A write annotation \033[31;1mInputs:\033[0m\n')
        printreg2(g, start_in, (arity / 5))
        g.write('A write annotation \033[31;1mOutputs:\033[0m\n')
        printreg2(g, start_out, (arity / 5))

        g.write('ret\n\n')

        g.write('.'+layername+'_feedforward:\n\n')

        for i in xrange(arity / 5):
            g.write('R002 = R'+str(1000 + i + start_in)[1:]+'\n')
            g.write('jsrn _unpack_floats\n')
            g.write('jsr __inplace_sigmoids\n')
            g.write('jsrn _pack_floats\n')
            g.write('R'+str(1000 + i + start_out)[1:]+' = R002\n')

        g.write('\nret\n\n')

        g.write('.'+layername+'_backprop:\n\n')

        for i in xrange(arity / 5):

            g.write('R002 = R'+str(1000 + i + start_out)[1:]+'\n')

            # If this is an output sigmoid neuron, we use the optimal (cross-entropy)
            # cost function which cancels out perfectly with sigma'(x). Hence, we
            # just copy the error backwards:
            if (finality == False):
                g.write('jsrn _unpack_floats\n')
                g.write('R'+str(1000 + start_scratch)[1:]+' = R003\n')
                g.write('R'+str(1001 + start_scratch)[1:]+' = R004\n')
                g.write('R'+str(1002 + start_scratch)[1:]+' = R005\n')
                g.write('R'+str(1003 + start_scratch)[1:]+' = R006\n')
                g.write('R'+str(1004 + start_scratch)[1:]+' = R007\n')
                g.write('R002 = R'+str(1000 + i + start_in)[1:]+'\n')
                g.write('jsrn _unpack_floats\n')
                g.write('jsr __inplace_sigmoid_primes\n')
                g.write('*\nL003\nL'+str(1000 + start_scratch)[1:]+'\n>20\nS003\n')
                g.write('*\nL004\nL'+str(1001 + start_scratch)[1:]+'\n>20\nS004\n')
                g.write('*\nL005\nL'+str(1002 + start_scratch)[1:]+'\n>20\nS005\n')
                g.write('*\nL006\nL'+str(1003 + start_scratch)[1:]+'\n>20\nS006\n')
                g.write('*\nL007\nL'+str(1004 + start_scratch)[1:]+'\n>20\nS007\n')
                g.write('jsrn _pack_floats\n')

            g.write('R'+str(1000 + i + start_in)[1:]+' = R002\n')

        g.write('\nret\n\n')


if (sys.argv[1] == 'sigmoid'):
    sigmoid(False)
elif (sys.argv[1] == 'sigmoidoutput'):
    sigmoid(True)
elif (sys.argv[1] == 'bipartite'):
    bipartite()
elif (sys.argv[1] == 'inputconv'):
    inputconv()
elif (sys.argv[1] == 'hiddenconv'):
    hiddenconv()
elif (sys.argv[1] == 'evaluate'):
    evaluate()
else:
    print("Unrecognised layer type: \033[1;31m"+sys.argv[1]+"\033[0m")
    exit(1)




