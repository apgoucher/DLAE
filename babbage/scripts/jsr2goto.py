#!/usr/bin/python

# Adam P. Goucher, January 2016

'''
jsr2goto.py -- Allows jumping to and returning from subroutines.
'''

import sys
import re


def sanitise_register(r):

    return str(int(r[1:] if (r[0] == 'R') else r) + 1000)[1:]


def convert_corpus(f, g):

    p1 = re.compile('(jsrn?) ([^ :]+)[ ]*\n')
    p2 = re.compile('(retn?)[ ]*\n')
    p3 = re.compile('(pop|push) R?([0-9]+)[ ]*\n')
    p4 = re.compile('(R[0-9]+)[ ]*=[ ]*(R?[0-9]+)[ ]*([%+*/-])[ ]*(R?[0-9]+)[ ]*\n')
    p5 = re.compile('(R[0-9]+)[ ]*=[ ]*(R?[0-9]+)[ ]*\n')
    p6 = re.compile('(memco?py|v[rl]shift)[ (]*R?([0-9]+)[ ,]*R?([0-9]+)[ ,]*([0-9]+)[ )]*\n')

    i = 0

    # Expand instructions 'jsr', 'ret', 'jsrn' and 'retn':
    for x in f:
        m1 = p1.match(x)
        m2 = p2.match(x)
        m3 = p3.match(x)
        m4 = p4.match(x)
        m5 = p5.match(x)
        m6 = p6.match(x)
        if m1:
            op = m1.group(1)
            labelname = m1.group(2)
            if (op == 'jsr'):
                i += 1
                g.write('N012 '+str(i)+'\n')
                i += 1
                g.write('N011 '+str(i)+'\n')
                g.write('jmp _stackpush\n')
                g.write('._callpoint_'+str(i)+':\n')
                g.write('jmp '+labelname+'\n')
                g.write('._callpoint_'+str(i-1)+':\n')
            else:
                i += 1
                g.write('N011 '+str(i)+'\n')
                g.write('jmp '+labelname+'\n')
                g.write('._callpoint_'+str(i)+':\n')
        elif m2:
            op = m2.group(1)
            if (op == 'ret'):
                g.write('jmp _ret_rec\n')
            else:
                g.write('jmp _ret_nrec\n')
        elif m3:
            op = m3.group(1)
            reg = sanitise_register(m3.group(2))
            if (op == 'push'):
                g.write('+\n')
                g.write('L000\n')
                g.write('L'+reg+'\n')
                g.write('S012\n')
                i += 1
                g.write('N011 '+str(i)+'\n')
                g.write('jmp _stackpush\n')
                g.write('._callpoint_'+str(i)+':\n')
            else:
                i += 1
                g.write('N011 '+str(i)+'\n')
                g.write('jmp _stackpop\n')
                g.write('._callpoint_'+str(i)+':\n')
                g.write('+\n')
                g.write('L000\n')
                g.write('L012\n')
                g.write('S'+reg+'\n')
        elif m4:
            outreg = sanitise_register(m4.group(1))
            op = m4.group(3)
            g.write(('/' if (op == '%') else op)+'\n')
            for operand in [m4.group(2), m4.group(4)]:
                if (operand[0] == 'R'):
                    g.write('L'+sanitise_register(operand[1:])+'\n')
                else:
                    g.write('N001 '+operand+'\n')
                    g.write('L001\n')
            g.write('S'+outreg+("'" if (op == '/') else "")+'\n')
        elif m5:
            outreg = sanitise_register(m5.group(1))
            inreg = m5.group(2)
            if (inreg[0] == 'R'):
                g.write('+\n')
                g.write('L000\n')
                g.write('L'+sanitise_register(inreg)+'\n')
                g.write('S'+outreg+'\n')
            else:
                g.write('N'+outreg+' '+inreg+'\n')
        elif m6:
            opname = m6.group(1)
            outbegin = int(sanitise_register(m6.group(2)))
            inbegin = int(sanitise_register(m6.group(3)))
            blocklen = int(m6.group(4))
            for j in xrange(blocklen):
                outreg = str(1000 + j + outbegin)[1:]
                inreg = str(1000 + j + inbegin)[1:]
                if (opname == 'vrshift'):
                    g.write('/\n')
                    g.write('L'+inreg+'\n')
                    g.write('L001\n')
                    g.write('S'+inreg+"'\n")
                    g.write('S'+outreg+'\n')
                elif (opname == 'vlshift'):
                    g.write('*\n')
                    g.write('L'+outreg+'\n')
                    g.write('L001\n')
                    g.write('S'+outreg+'\n')
                    g.write('+\n')
                    g.write('L'+inreg+'\n')
                    g.write('L'+outreg+'\n')
                    g.write('S'+outreg+'\n')
                else:
                    g.write('+\n')
                    g.write('L000\n')
                    g.write('L'+inreg+'\n')
                    g.write('S'+outreg+'\n')
        else:
            g.write(x)

    return i


def write_subroutine_implementation(g, numpoints, recursive=True):

    i = numpoints

    # Subroutines for returning from subroutines:
    g.write('\n\n\n')

    if recursive:
        g.write('._ret_rec:\n')
        i += 1
        g.write('N011 '+str(i)+'\n')
        g.write('jmp _stackpop\n')
        g.write('._callpoint_'+str(i)+':\n')
        g.write('+\n')
        g.write('L000\n')
        g.write('L012\n')
        g.write('S011\n')

    g.write('._ret_nrec:\n')

    # Ensure a valid address is specified:
    g.write('N001 1\n')
    g.write('-\n')
    g.write('L011\n')
    g.write('L001\n')
    g.write('cjmp _callpoint_out_of_bounds\n')
    g.write('N001 '+str(i+1)+'\n')
    g.write('-\n')
    g.write('L001\n')
    g.write('L011\n')
    g.write('cjmp _callpoint_out_of_bounds\n')

    # Binary tree:
    tree_return(g, 1, i+1)

    # If an invalid address was specified:
    g.write('._callpoint_out_of_bounds:\n')
    g.write('A write in columns\n')
    g.write('A write annotation Error: cannot return to invalid address (\n')
    g.write('L011\n')
    g.write('P\n')
    g.write('A write annotation  must be between 1 and '+str(i)+')\n')
    g.write('A write new line\n')
    g.write('A write in rows\n')
    g.write('H fatal error\n')


def write_stack_implementation(g, stacksize):

    g.write('\n\n\n')
    g.write('._stackpush:\n')
    g.write('N001 '+str(13+stacksize)+'\n')
    g.write('-\n')
    g.write('L001\n')
    g.write('L013\n')
    g.write('cjmp _stack_overflow\n')
    tree_return(g, 14, 14+stacksize, 'push')
    g.write('._finished_pushing:\n')
    g.write('N001 1\n')
    g.write('+\n')
    g.write('L013\n')
    g.write('L001\n')
    g.write('S013\n')
    g.write('jmp _ret_nrec\n')

    g.write('._stack_overflow:\n')
    g.write('A write in rows\n')
    g.write('A write annotation Error: stack overflow (cannot push to full stack)\n')
    g.write('H fatal error\n')

    g.write('\n\n\n')
    g.write('._stackpop:\n')
    g.write('N001 15\n')
    g.write('-\n')
    g.write('L013\n')
    g.write('L001\n')
    g.write('cjmp _stack_underflow\n')
    g.write('N001 1\n')
    g.write('-\n')
    g.write('L013\n')
    g.write('L001\n')
    g.write('S013\n')
    tree_return(g, 14, 14+stacksize, 'pop')
    g.write('._finished_popping:\n')
    g.write('jmp _ret_nrec\n')

    g.write('._stack_underflow:\n')
    g.write('A write in rows\n')
    g.write('A write annotation Error: stack underflow (cannot pop from empty stack)\n')
    g.write('H fatal error\n')

def tree_return(g, left, right, mode='ret'):
    '''
    Implements Julian Brown's binary tree idea for addressing locations
    in memory and in the program.
    '''

    midpoint = (left + right) / 2
    if (midpoint == left):
        if (mode == 'ret'):
            g.write('jmp _callpoint_'+str(midpoint)+'\n')
        elif (mode == 'push'):
            g.write('+\n')
            g.write('L000\n')
            g.write('L012\n')
            g.write('S'+str(1000 + midpoint)[1:]+'\n')
            g.write('jmp _finished_pushing\n')
        elif (mode == 'pop'):
            g.write('+\n')
            g.write('L000\n')
            g.write('L'+str(1000 + midpoint)[1:]+'\n')
            g.write('S012\n')
            g.write('jmp _finished_popping\n')
        else:
            print('Invalid tree_return mode')
            exit(1)
    else:
        g.write('N001 '+str(midpoint)+'\n')
        g.write('-\n')
        if (mode == 'ret'):
            g.write('L011\n')
        else:
            g.write('L013\n')
        g.write('L001\n')
        g.write('cjmp _'+mode+'_less_than_'+str(midpoint)+'\n')
        tree_return(g, midpoint, right, mode)
        g.write('._'+mode+'_less_than_'+str(midpoint)+':\n')
        tree_return(g, left, midpoint, mode)


def main():

    if (len(sys.argv) < 3):
        print("Usage: python jsr2goto.py infile.xcf outfile.xcf [stacksize]")
        exit(1)

    infile = sys.argv[1]
    outfile = sys.argv[2]
    stacksize = int(sys.argv[3]) if (len(sys.argv) == 4) else 16

    with open(infile, 'r') as f:
        with open(outfile, 'w') as g:

            g.write('   Initialise variables:\n')
            g.write('N000 0\n\n')
            if (stacksize > 0):
                g.write('   Stack pointer:\n')
                g.write('N013 14\n\n')

            i = convert_corpus(f, g)
            write_subroutine_implementation(g, i, (stacksize>0))
            if (stacksize > 0):
                write_stack_implementation(g, stacksize)

    print('Successfully expanded cardfile '+infile+' to '+outfile+' ('+str(i+1)+' callpoints)')


main()
